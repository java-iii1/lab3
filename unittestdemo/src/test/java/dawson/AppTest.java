package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * @ * Rigorous Test :-)
     */

    @Test
    public void echoTest() {

        assertEquals("The result of echo should be 5", App.echo(5), 5);
    }

    @Test
    public void oneMoreTest() {
        App p = new App();
        assertEquals("add one more", 6, p.oneMore(5));
    }
}
